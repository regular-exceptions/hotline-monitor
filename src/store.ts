import Vue from 'vue';
import firebase from 'firebase/app';
import 'firebase/firestore';

// Initialize Firebase, copy this from the cloud console
// Or use mine :)
const config = {
  apiKey: 'AIzaSyBoBFuOCZ6HQ-bjt-L7dZ-El9zHv5NiJ4A',
  authDomain: 'gesundheitsamtcui-jopkpi.firebaseapp.com',
  databaseURL: 'https://gesundheitsamtcui-jopkpi.firebaseio.com',
  projectId: 'gesundheitsamtcui-jopkpi',
  storageBucket: 'gesundheitsamtcui-jopkpi.appspot.com',
  messagingSenderId: '858273665267',
  appId: '1:858273665267:web:c2eede3da608399be3e786'
};
firebase.initializeApp(config);

// The shared state object that any vue component can get access to.
// Has some placeholders that we’ll use further on!
const entries: string[] = ['Moep'];
export const store = {
  entries,
};

const firestore = firebase.firestore();
console.log('firestore: ', firestore);
const dialogFlowCollection = firestore
  .collection('dialogflow');
dialogFlowCollection
  .onSnapshot((agentRef) => {
    agentRef.forEach((doc) => {
      const docData = doc.data();
      store.entries.push(docData.entry);
    });
  });
